# README #

### What is this repository for? ###
A simple application to show desktop notification  
for any news item added to the RSS feed of  
specified news source.

### How do I get set up? ###
pip install notify2  
pip install requests

To run:  
(foreground) $$ python notifier.py  
(background) $$ python notifier.py &  

### Contribution guidelines ###
Everyone is invited to add features to this mini project  
and expand it's functionalities.

### Who do I talk to? ###
Admin: Rohit Chugh  
Email: rohitchugh.95@gmail.com